# TTU Bag Valve Mask Ventilator

This project is the collaboration of Frenship High School student Mark Dannemiller with a team of Texas Tech staff and students from several departments.  The aim was to make an affordable but 
easily manufacturable ventilator from parts commonly found in FRC shops and stateside with FRC vendors. 

[Read more here](https://gitlab.com/MarkDannemiller/ttu-bvm-paddle-ventilator/-/wikis/home)

[Texas Tech Article on COVID-19 efforts and other ventilator projects within the college](https://today.ttu.edu/posts/2020/04/Stories/engineers-designing-building-ventilators)

## [Contributers](https://gitlab.com/MarkDannemiller/ttu-bvm-paddle-ventilator/wikis/Contributers)